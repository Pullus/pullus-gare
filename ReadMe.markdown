Generic Share Extension
=======================

The **G**eneric sh**ARE** extension (GARE) is a MacOS[*](#requirements) Share Extension that can be customized very easily with your own (bash) scripts.

Within your application, select the GARE extension (via the Share Button) to select and apply one of your scripts.

![GARE dialog](./Documentation/Gare-Share-Extension-Dialog.png)

The GARE extension retrieves an URL from your application and calls the script with this URL. If there are multiple URLs, the script is called for each URL.

GARE Application
----------------

The GARE application is necessary to provide the GARE extension.

![GARE application](./Documentation/Gare-Application.png)

It has only two functions:

 - a list that shows the available scripts and
 - a button to jump to the script folder.

Customization
-------------
In order for GARE to offer and execute a script

 - it must be stored in `'~/Library/Application Support/Gare'`
 - it should use `.sh` as file extension
 - it must be executable
 - it can have a custom (Finder) icon that will be displayed by GARE
 - it can access the URL from the host application via the first and only argument

### Example: Open Firefox

The script folder `'~/Library/Application Support/Gare'`

![Script folder](./Documentation/Finder-Application-Support-Gare.png)
 
contains the `'Open Firefox.sh'` script

```
#!/bin/bash
open -a Firefox "$@"
```

that is just a simple wrapper to open Firefox.

Limitations
-----------

The current implementation supports only URLs and bash scripts.

Requirements <a id="requirements"/>
------------

Developed and tested with MacOS 10.12.

This software was intentionally developed for macOS 10.12 to be able to continue using older devices.

Developer Notes
---------------

The GARE application and the GARE extension must be build with Xcode. A Swift Package Manager build is not implemented.
