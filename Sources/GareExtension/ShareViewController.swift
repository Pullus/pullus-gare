import Cocoa
import os.log

import PullusBase

import GareShared

///
///
///
class ShareViewController : NSViewController, OrderTableViewDelegate {
	
	// MARK: - XPC Connection
	
	///
	/// The XPCConnection to the service.
	///
	private
	let connection = XPCConnection<GareServiceProtocol>.initGareConnection()
		|- { $0.resume() }
	
	///
	/// The remote service.
	///
	private
	var gareService: GareServiceProtocol? { return connection.remoteInstanceProxy(mode: .asynchronous) }
	
	// MARK: - NSViewController
	
	override
	var nibName: NSNib.Name? { return NSNib.Name("ShareViewController") }
	
	///
	/// The view with the provided orders that could be applied to the URLs.
	///
	@IBOutlet weak
	var orderTableView: OrderTableView!
	
	///
	/// Sets self as the delegate of the order table view.
	///
	override
	func awakeFromNib() {
		super.awakeFromNib()
		orderTableView.orderTableViewDelegate = self
	}
	
	///
	/// Updates the order table view with the orders from the gare service.
	///
	override
	func viewDidLoad() {
		super.viewDidLoad()
		
		updateOrderTableView()
	}
	
	///
	/// Retrieves the orders from the service and adds them to the view.
	///
	private
	func updateOrderTableView() {
		gareService?.withOrders { orders in
			orderTableView.orders = orders.sorted { $0.title < $1.title }
		}
	}
	
	///
	/// Cancels the share extension.
	///
	@IBAction
	func cancel(_ sender: AnyObject?) {
		let cancelError = NSError(domain: NSCocoaErrorDomain, code: NSUserCancelledError, userInfo: nil)
		
		extensionContext?.cancelRequest(withError: cancelError)
	}
	
	// MARK: - OrderTableViewDelegate
	
	///
	/// Calls the service with the URLs collected from the extension context and
	/// quits.
	///
	func handle(orderTableView anOrderTableView: OrderTableView, hasSelected anOrder: Order) {
		extensionContext?.providers().forURLs(
			do: { url in self.gareService?.execute(order: anOrder, url: url) },
			failure: { error in os_log("Cannot read the URL from the ItemProvider.", type: .error) } )
		
		let outputItem = NSExtensionItem()
		// Complete implementation by setting the appropriate value on the output item
		
		let outputItems = [outputItem]
		extensionContext?.completeRequest(returningItems: outputItems, completionHandler: nil)
	}
	
}
