import Foundation

public
extension FileManager {
	
	///
	/// The application support directory of the user for all applications.
	///
	/// This should be `~/Library/Application Support/`
	///
	var applicationSupportDirectory: URL {
		return urls(for: .applicationSupportDirectory, in: .userDomainMask).first ?? FileManager.devNull
	}

}
