import os.log
import Foundation

public
extension FileManager {
	
	///
	/// The application support directory of the user for 'this' application.
	///
	/// E.g.: "~/Libaray/Application Support/Xcode" for the 'Xcode' app.
	///
	public static
	var applicationSupportApplicationDirectory: URL {
		guard let bundleName = Bundle.applicationName else {
            os_log("Can't determine the application support application directory, because the name of the main bundle is unknown.")
			return FileManager.devNull
		}
		
		return FileManager.default
			.urls(for: .applicationSupportDirectory, in: .userDomainMask).first?
			.appendingPathComponent(bundleName, isDirectory: true)
			?? FileManager.devNull
	}
	
}
