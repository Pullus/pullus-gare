import Foundation

public
extension FileManager {
	
	///
	/// The URL of '/dev/null'.
	///
	public
	static let devNull: URL = URL(fileURLWithPath: "/dev/null")
	
}
