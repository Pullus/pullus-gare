import Foundation

extension NSXPCConnection {
	
	///
	/// The connection mode for methods.
	///
	/// Apperently the mode effects only methods with a callback like
	/// 'func getMessage(reply aReply: (message) -> Void)'.
	/// Methods with no callback are always as asynchronous.
	///
	public
	enum Mode {
		///
		/// For asynchronous callbacks.
		///
		case asynchronous
		
		///
		/// For synchronous callbacks.
		///
		case synchronous
	}
	
	///
	/// Creates a new remote object proxy.
	///
	public
	func remoteObjectProxy(mode aMode: Mode = .asynchronous, errorHandler anErrorHandler: ErrorHandler? = nil ) -> Any? {
		let errorHandler = anErrorHandler ?? { error in
			NSLog("The NSXPCConnection to '\(self.serviceName ?? "?")' for the remote object interface '\(String(describing: self.remoteObjectInterface))' failed. Error is: '\(error.localizedDescription)'.")
		}
		
		switch aMode {
		case .asynchronous: return self.remoteObjectProxyWithErrorHandler(errorHandler)
		case .synchronous:  return self.synchronousRemoteObjectProxyWithErrorHandler(errorHandler)
		}
	}
	
}
