import Foundation

extension NSXPCInterface {
	
	///
	/// Allows a class as an argument.
	///
	public
	func add(class aClass: AnyClass, for aSelector: Selector, at anArgumentIndex: Int, ofReply anOfReply: Bool) {
		let allowedClasses = classes(for: aSelector, argumentIndex: anArgumentIndex, ofReply: anOfReply) as NSSet
		
		setClasses(allowedClasses.adding(aClass) as Set<NSObject>, for: aSelector, argumentIndex: anArgumentIndex, ofReply: anOfReply)
	}
	
}
