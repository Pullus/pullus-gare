import Foundation

public
extension Data {
	
	public
	func toNSData() -> NSData? {
		return NSData(data: self)
	}
	
}
