import Foundation

extension Sequence where Element == NSItemProvider {
	
	///
	/// Filters and iterates over a sequence and calls `withURL` for every URL.
	///
	/// A typesafe convenience method of `loadItem` for an URL.
	///
	/// See: https://developer.apple.com/library/archive/documentation/Miscellaneous/Reference/UTIRef/Articles/System-DeclaredUniformTypeIdentifiers.html
	//
	public
	func forURLs(do aBlock: @escaping (URL) -> Void, failure: ErrorHandler? = nil) {
		self
			.filter { $0.hasItemConformingToTypeIdentifier(URL.uniformTypeIdentifier) }
			.forEach { $0.withURL(do: aBlock, failure: failure ) }
	}
	
}
