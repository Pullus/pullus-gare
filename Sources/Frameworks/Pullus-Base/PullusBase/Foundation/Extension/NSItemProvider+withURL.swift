import Foundation

extension NSItemProvider {
	
	///
	/// Loads an URL item and calls a block or an error handler.
	///
	/// A typesafe convenience method of `loadItem` for an URL.
	///
	public
	func withURL(do aBlock: @escaping (URL) -> Void, failure: ErrorHandler? = nil) {
		loadItem(forTypeIdentifier: URL.uniformTypeIdentifier) { item, error in
			guard let url = item as? URL else {
				failure?(error)
				return
			}
			aBlock(url)
		}
	}
	
}
