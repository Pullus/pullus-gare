import Foundation

public
extension NSExtensionContext {
	
	///
	/// Returns the providers of all input/extension items.
	///
	public
	func providers() -> [NSItemProvider] {
		return inputItems
			// Filter and cast to NSExtensionItem
			.compacted { item in item as? NSExtensionItem }
			
			// Collect attachments
			.flatMap { item in item.attachments }
			.flatMap { $0 }
			
			// Filter and cast to NSItemProvider
			.compacted { attachment in attachment as? NSItemProvider }
	}
}
