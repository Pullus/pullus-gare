import Foundation

extension URL {
	
	///
	/// The Uniform Type Identifier (UTI) of an URL
	///
	public static
	let uniformTypeIdentifier = kUTTypeURL as String
	
}
