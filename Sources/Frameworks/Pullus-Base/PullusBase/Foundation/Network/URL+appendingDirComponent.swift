import Foundation

public
extension URL {
	
	///
	/// Returns an URL constructed by appending the given dir path to self.
	///
	public
	func appendingDirComponent(_ aDirComponent: String) -> URL {
		return appendingPathComponent(aDirComponent, isDirectory: true)
	}

}
