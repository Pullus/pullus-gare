import Foundation

public
extension Bundle {
	
	///
	/// The name of the application (name of the main Bundle).
	///
	public
	static var applicationName: String? {
		return Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
	}
}
