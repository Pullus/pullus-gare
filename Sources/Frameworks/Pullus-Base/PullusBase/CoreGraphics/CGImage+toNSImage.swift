import AppKit

public
extension CGImage {
	
	public
	func toNSImage() -> NSImage {
		return NSImage(
			cgImage: self,
			size: CGSize(width: CGFloat(width), height: CGFloat(height)))
	}
	
}
