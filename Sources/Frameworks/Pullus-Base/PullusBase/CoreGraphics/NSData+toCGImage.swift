import Foundation

public
extension NSData {
	
	///
	/// Creates an icon.
	///
	/// See `CGImageSourceCreateWithData` and `CGImageSourceCreateImageAtIndex`
	/// for argument description.
	///
	public
	func toCGImage(sourceOptions: CFDictionary? = nil, imageIndex: Int = 0, imageOptions: CFDictionary? = nil) -> CGImage? {
		guard let source = CGImageSourceCreateWithData(self, sourceOptions) else { return nil }
		
		return CGImageSourceCreateImageAtIndex(source, imageIndex, imageOptions)
	}
	
}
