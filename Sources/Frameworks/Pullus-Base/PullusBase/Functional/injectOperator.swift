///
/// The 'inject' operator.
///
infix operator |- : InjectOperator

///
/// Precedence group for the forward pipe operator.
///
/// HINT: Do not use it. Could be removed.
///
precedencegroup InjectOperator {
	higherThan: NilCoalescingPrecedence
	associativity: left
}

///
/// The inject operator.
///
/// Example:
/// <code>
/// let dateFormatter = DateFormatter()
///							|- { $0.dateStyle = .medium }
///							|- { $0.timeStyle = .none }
/// </code>
/// or
/// <code>
/// let dateFormatter = DateFormatter() |- {
///							$0.dateStyle = .medium
///							$0.timeStyle = .none
///							}
/// </code>
///
public
func |- <Object: AnyObject> (_ anObject: Object, _ aBlock: (Object) throws -> Void) rethrows -> Object {
	try aBlock(anObject)
	
	return anObject
}
