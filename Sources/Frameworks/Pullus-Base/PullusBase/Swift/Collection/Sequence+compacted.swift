public
extension Sequence where Element : OptionalType {
	
	///
	/// Returns a sequence without nil-values.
	///
	func compacted() -> [Element.Wrapped] {
		return self
			// The wrapped or nil elements
			.map { $0 as? Element.Wrapped }
			
			// Sort out nil values
			.filter { $0 != nil }
			
			// Force
			.map { $0! }
	}
}
