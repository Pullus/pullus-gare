public
extension Sequence {
	
	///
	///
	///
	func compacted<Result>(_ aMap: (Element) throws -> Result?) rethrows -> [Result] {
		return try self
			// Apply mapping
			.map(aMap)
			
			// Filter nil-values
			.filter { $0 != nil }
			
			// Unwrap
			.map { $0! }
	}
}
