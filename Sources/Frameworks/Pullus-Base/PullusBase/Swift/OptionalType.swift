import Foundation

///
/// A protocol of the Optional enum used to build constraints.
///
public
protocol OptionalType {
	
	associatedtype Wrapped
	
	///
	/// The map function from the Optional.
	///
	func map<Result>(_ format: (Wrapped) throws -> Result) rethrows -> Result?
}

///
/// The Optional extends the Optional Type.
///
extension Optional : OptionalType {
}
