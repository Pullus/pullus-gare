import XCTest
@testable import PullusBase

class URL_appendingDirComponentTests : XCTestCase {
	
	let url = URL(fileURLWithPath: "tmp", isDirectory: true)
	
	func test() {
		let result = url.appendingDirComponent("cache")
		
		XCTAssertTrue(result.hasDirectoryPath)
		XCTAssertEqual("tmp/cache", result.relativePath)
	}
}
