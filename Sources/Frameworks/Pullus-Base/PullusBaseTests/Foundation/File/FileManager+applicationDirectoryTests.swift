import XCTest
@testable import PullusBase

class FileManager_applicationDirectoryTests : XCTestCase {
	
	func test() {
		let expected = FileManager.default
			.homeDirectoryForCurrentUser
			.appendingPathComponent("Library", isDirectory: true)
			.appendingPathComponent("Application Support", isDirectory: true)
		
		XCTAssertEqual(expected, FileManager.default.applicationSupportDirectory)
	}
}
