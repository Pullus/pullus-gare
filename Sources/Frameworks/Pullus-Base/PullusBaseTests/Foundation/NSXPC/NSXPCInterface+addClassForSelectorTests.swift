import XCTest
@testable import PullusBase

class NSXPCInterface_addClassForSelectorTests : XCTestCase {

	let interface = NSXPCInterface()
	let selector = #selector(TestServiceProtocol.save(customObject:))

	override func setUp() {
		interface.protocol = TestServiceProtocol.self
	}
	
	func test() {
		interface.add(class: CustomClass.self, for: selector, at: 0, ofReply: false)
		
		let classes = interface.classes(for: selector, argumentIndex: 0, ofReply: false) as NSSet
		XCTAssertTrue(classes.contains(CustomClass.self))
	}
}
