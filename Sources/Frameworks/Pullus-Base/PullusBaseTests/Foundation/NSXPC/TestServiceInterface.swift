import Foundation

///
/// The service protocol
///
@objc
protocol TestServiceProtocol {
	
	@objc func save(customObject: CustomClass)
}
