import XCTest
@testable import PullusBase

class XPCConnectionTests : XCTestCase {
	
	let serviceName = "XPCConnectionTest"
	
	class NotAProtocol {
	}
	
	//
	func testInitWithWrongInterface() {
		let connection = XPCConnection<NotAProtocol>(serviceName: serviceName)

		// TODO: Test NSLog
		XCTAssertEqual(serviceName, connection.serviceName)
	}
	
	func testInit() {
		let connection = XPCConnection<TestServiceProtocol>(serviceName: serviceName)
		
		XCTAssertEqual(serviceName, connection.serviceName)
	}
	
	// TODO: test remoteInstanceProxy
}
