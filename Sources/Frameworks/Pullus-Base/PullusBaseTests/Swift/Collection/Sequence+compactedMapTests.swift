import XCTest
@testable import PullusBase

class Sequence_compactedMapTests : XCTestCase {
	
	let values = [ 1, 2, 3, 4 ]
	
	func test() {
		let result: [Double] = values.compacted { $0 > 2 ? Double($0) : nil }
		
		XCTAssertEqual([3.0, 4.0], result)
	}
}
