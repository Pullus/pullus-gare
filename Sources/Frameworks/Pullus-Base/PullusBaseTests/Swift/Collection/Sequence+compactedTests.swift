import XCTest
@testable import PullusBase

class Sequence_compactedTests : XCTestCase {
	
	let values = [ 1, 2, nil, 4 ]
	
	func test() {
		XCTAssertEqual([1, 2, 4], values.compacted())
	}
}
