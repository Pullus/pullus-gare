import XCTest
@testable import PullusBase

class Sequence_mapToDictionaryTests : XCTestCase {
	
	struct Person {
		let name: String
		let age: Int
	}
	
	let persons = [
		Person(name: "Peter", age: 32),
		Person(name: "Jane", age: 32),
		Person(name: "John", age: 34),
		Person(name: "Tom", age: 8)
	]
	
	let names = [ "Peter", "Jane", "John", "Tom" ]
	let ages = [32, 30, 34, 8 ]
	
	func testMapping() {
		let expected = [ "Peter": 32, "Jane": 32, "John": 34, "Tom": 8 ]
		let result = persons.mapToDictionary { person in return (person.name, person.age) }
		
		XCTAssertEqual(expected, result)
	}
	
	func testOptionalKey() {
		let persons = [ Person(name: "Peter", age: 32),
						nil,
						Person(name: "John", age: 34),
						Person(name: "Tom", age: 8)
		]
		let expected = [ "Peter": 32, "John": 34, "Tom": 8 ]

		let result = persons.mapToDictionary { person in return (person?.name, person?.age ?? 0) }
		
		XCTAssertEqual(expected, result)
	}
	
	func testOverride() {
		let expected = [ 32: "Jane", 34: "John", 8: "Tom" ]
		let result = persons.mapToDictionary { person in return (person.age, person.name) }
		
		XCTAssertEqual(expected, result)
	}
}
