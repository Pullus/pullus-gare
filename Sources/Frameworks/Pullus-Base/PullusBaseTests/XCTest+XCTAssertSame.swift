import XCTest

///
/// Assert identity.
///
func XCTAssertSame(_ anObjectA: AnyObject?, _ anObjectB: AnyObject?, file aFile: StaticString = #file, line aLine: UInt = #line) {
	XCTAssertTrue(anObjectA === anObjectB, file: aFile, line: aLine)
}
