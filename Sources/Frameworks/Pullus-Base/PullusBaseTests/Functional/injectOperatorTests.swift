import XCTest
@testable import PullusBase

class InjectOperatorTests : XCTestCase {
	
	class Person {
		var firstName: String
		
		init(firstName aFirstName: String) {
			firstName = aFirstName
		}
	}
	
	func testIdentity() {
		let person = Person(firstName: "Jane")
		
		let peter: Person = person |- { _ in /* Do nothing */ }
		
		XCTAssertSame(person, peter)
	}
	
	func testBlock() {
		let peter: Person = Person(firstName: "Jane") |- { $0.firstName = "Peter" }
		
		XCTAssertEqual("Peter", peter.firstName)
	}
	
	func testException() {
		enum Failure: Error { case failure }
		
		do {
			let _: Person = try Person(firstName: "Peter") |- { _ in throw Failure.failure }
			XCTFail("No throw")
		} catch {
			XCTAssertTrue(true)
		}
	}
}
