#import <Cocoa/Cocoa.h>

// Project version number for PullusBaseMacOS.
FOUNDATION_EXPORT double PullusBaseMacOSVersionNumber;

// Project version string for PullusBaseMacOS.
FOUNDATION_EXPORT const unsigned char PullusBaseMacOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PullusBaseMacOS/PublicHeader.h>
