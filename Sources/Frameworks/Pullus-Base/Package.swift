// swift-tools-version:4.0
import PackageDescription
import Foundation

///
/// Build this package with minimum platform MacOS 10.12 by using the `target`
/// `x86_64-apple-macosx10.12`
///
/// Call
/// ```
///    swift build -Xswiftc "-target" -Xswiftc "x86_64-apple-macosx10.12"
/// ```
///
// swiftlint:disable trailing_comma
let package = Package(
	name: "PullusBase",
	
	// Swift Tools Version 4.0 doesn't support `platforms`
	//	platforms: [
	//		.macOS(.v12)
	//	],
	products: [
		.library(
			name: "PullusBase",
			targets: ["PullusBase-macOS"]),
		],
	
	dependencies: [
	],
	
	targets: [
		.target(
			name: "PullusBase-macOS",
			dependencies: [
			],
			path: "PullusBase"),
		]
)
// swiftlint:enable trailing_comma
