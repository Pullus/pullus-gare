import Foundation

///
/// Helper to configure a NSXPCInterface with the service protocol.
///
public
extension NSXPCInterface {
	
	///
	/// Adds the allowed/expected custom types of the service protocol methods
	/// to a `NSXPCInterface`.
	///
	/// This method should be called by the consumer of a service.
	///
	/// For arguments of collection types (e.g. Array, Dictionary) this is
	/// mandatory.
	///
	public
	func configureGareServiceProtocol() {
		add(class: Order.self,	for: #selector(GareServiceProtocol.withOrders(reply:)), at: 0, ofReply: true)
		add(class: NSUUID.self,	for: #selector(GareServiceProtocol.withOrders(reply:)), at: 0, ofReply: true)
		
		add(class: NSUUID.self,	for: #selector(GareServiceProtocol.execute(id:url:)), at:0, ofReply: false)
		add(class: NSURL.self,	for: #selector(GareServiceProtocol.execute(id:url:)), at:1, ofReply: false)
	}
	
}
