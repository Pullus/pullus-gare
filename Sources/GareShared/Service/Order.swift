import Foundation
import AppKit

///
/// The meta information of an action.
///
/// It is used to
///  - identify an action and
///  - to decouple the handling of an action from its implementation
///
/// The Gare serivce protocol contains `Order`s -- no `Action`s.
///
/// The XPC Gare service manages, stores and executes multiple actions. In
/// general, these actions cannot be sent through a XPC connection between the
/// Gare service and its clients. Therefore `Orders`s are used as a
/// substitute in the service protocol to allow clients to handle/access
/// actions.
///
/// The `id`, the `title` and the `icon` are copied from the action.
///
@objc(Order)
public
class Order : NSObject, NSSecureCoding {
	
	///
	/// See `Action`.
	///
	public let id: ActionID
	
	///
	/// See `Action`.
	///
	public let title: String
	
	///
	/// See `Action`.
	///
	public let icon: NSImage?
	
	// MARK: - NSSecureCoding for XPC connection
	
	public class var supportsSecureCoding: Bool { return true }
	
	struct Key {
		static let id = "id"
		static let title = "title"
		static let icon = "icon"
	}
	
	public required
	init?(coder aDecoder: NSCoder) {
		guard let i = aDecoder.decodeObject(forKey: Key.id) as? ActionID else { return nil }
		id = i
		
		guard let t = aDecoder.decodeObject(forKey: Key.title) as? String else { return nil }
		title = t
		
		icon = (aDecoder.decodeObject(forKey: Key.icon) as? NSData)?
			.toCGImage()?
			.toNSImage()
	}
	
	public
	func encode(with aCoder: NSCoder) {
		aCoder.encode(id, forKey: Key.id)
		aCoder.encode(title, forKey: Key.title)
		aCoder.encode(icon?.tiffRepresentation?.toNSData(), forKey: Key.icon)
	}
	
	// MARK: -
	
	public
	init(id anID: ActionID, title aTitle: String, icon anIcon: NSImage) {
		id = anID
		title = aTitle
		icon = anIcon
	}
	
}
