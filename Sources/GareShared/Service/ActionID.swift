import Foundation

///
/// An alias for the action id.
///
public typealias ActionID = NSNumber
