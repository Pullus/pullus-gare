import Foundation

import PullusBase

public
extension XPCConnection where ObjectProtocol == GareServiceProtocol {
	
	private typealias SELF = XPCConnection
	
	private static
	let serviceName: String = "org.pullus.gare.GareService"
	
	///
	/// Create and configure a connection to an instance of
	/// `GareServiceProtocol`.
	///
	public
	static func initGareConnection() -> XPCConnection<GareServiceProtocol> {
		return XPCConnection<GareServiceProtocol>(serviceName: SELF.serviceName)
			|- { $0.remoteObjectInterface?.configureGareServiceProtocol() }
	}
	
}
