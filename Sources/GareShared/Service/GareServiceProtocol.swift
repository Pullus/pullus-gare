import Foundation

///
/// The interface to the XPC Gare service.
///
@objc public
protocol GareServiceProtocol {
	
	///
	/// Fetches the orders.
	///
	func withOrders(reply aReply: ([Order]) -> Void)
	
	///
	/// Executes an action with a given id and a parameter (URL).
	///
	/// The action id must be retrieved from an order.
	///
	/// Hint:
	/// There is no (remote) `execute(order…` on service level to avoid sending
	/// the order back to the service instance.
	///
	/// **Use the (local) protocol extension `execute(order: Order, url: URL)`**
	///
	func execute(id anID: ActionID, url anURL: URL)
}

extension GareServiceProtocol {

	///
	/// Convenience method to call `execute(id…`.
	///
	public
	func execute(order: Order, url: URL) {
		execute(id: order.id, url: url)
	}

}
