import Foundation

public
struct Globals {
	
	///
	/// The folder that stores the provided actions.
	///
	public static
	let scriptDirectory = FileManager.default
		.applicationSupportDirectory
		.appendingDirComponent("Gare")
	
}
