import os.log
import Foundation
import Cocoa

///
/// A special `NSTableView` of selectable `Order`s.
///
/// Used to display and trigger the execution of an `Order`.
///
/// This `NSTableView` encapsulates a view, a controller and a model.
///  - It stores a list of orders by implementing a `NSTableViewDataSource` for
///    itself (Model)
///  - It creates the table cells (rows) by implementing a `NSTableViewDelegate`
///    for itself (View)
///  - It handles double actions (clicks) by listining to itself (Controller)
///  - It supports a delegate that is informed about a confirmation of a
///    selected order. "The action of the order should be executed"
///
public
class OrderTableView : NSTableView, NSTableViewDataSource, NSTableViewDelegate {
	
	private typealias SELF = OrderTableView
	
	// MARK: - NSTableView
	
	///
	/// Initializes `delegate` and `dataSource` with `self`.
	///
	public override
	func awakeFromNib() {
		// See: https://stackoverflow.com/questions/5374076/nsview-initialization-init-vs-awakefromnib#5374293
		super.awakeFromNib()
		
		delegate = self
		dataSource = self
		
		// Listen to itself
		target = self
		doubleAction = #selector(handleConfirmSelectedOrder(_:))
	}
	
	// MARK: - NSTableViewDataSource
	
	///
	/// Changing the orders triggers a reload/refresh of the view.
	///
	public
	var orders: [Order] = [] {
		didSet { reloadData() }
	}
	
	///
	/// The number of orders.
	///
	public
	func numberOfRows(in tableView: NSTableView) -> Int {
		return orders.count
	}
	
	// MARK: - NSTableViewDelegate
	
	///
	/// The identfier of the (reusable) `NSTableCellView` that is defined
	/// in the table view with the InterfaceBuilder.
	///
	private static
	let orderCellViewIdentifier = NSUserInterfaceItemIdentifier("OrderCellView")
	
	///
	/// Generates a row for each order.
	///
	public
	func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
		guard
			let cell = tableView.makeView(withIdentifier: SELF.orderCellViewIdentifier, owner: nil) as? NSTableCellView
			else {
				os_log("Can't create table row (cell). The table doesn't contain an 'OrderCellView' template that can be used for the rows.", type: .error)
				return nil
		}
		
		let order = orders[row]
		
		cell.textField?.stringValue = order.title
		cell.imageView?.image = order.icon
		
		return cell
	}
	
	// MARK: - OrderTableViewDelegate
	
	///
	/// The delegate that should receive `handle(orderTableView:...` messages
	/// from this instance.
	///
	public weak
	var orderTableViewDelegate: OrderTableViewDelegate?
	
	///
	/// Is called from a "double action" (table view) or an "Ok" button to
	/// inform the delegate that a seletion of an order has been confirmed.
	///
	@IBAction
	func handleConfirmSelectedOrder(_ send: Any?) {
		orderTableViewDelegate?.handle(orderTableView: self, hasSelected: orders[selectedRow])
	}
	
}
