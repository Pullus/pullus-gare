///
///
///
public
protocol OrderTableViewDelegate : class {
	
	///
	/// This delegate of an `OrderTableViewDelegate` should handle that a
	/// selected order has been confirmed.
	///
	func handle(orderTableView anOrderTableView: OrderTableView, hasSelected anOrder: Order)
	
}
