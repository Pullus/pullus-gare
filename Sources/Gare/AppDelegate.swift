import Cocoa

@NSApplicationMain
class AppDelegate : NSObject, NSApplicationDelegate {
		
	// See: https://nabtron.com/quit-cocoa-app-window-close/
	func applicationShouldTerminateAfterLastWindowClosed (_ theApplication: NSApplication) -> Bool {
		return true
	}
	
}
