import os.log
import Foundation
import Cocoa

import PullusBase

import GareShared

class ViewController : NSViewController {
	
	// MARK: - XPC Connection

	///
	/// The XPCConnection to the service.
	///
	private
	let connection = XPCConnection<GareServiceProtocol>.initGareConnection()
		|- { $0.resume() }
	
	///
	/// The remove service
	///
	private
	var gareService: GareServiceProtocol? { return connection.remoteInstanceProxy(mode: .asynchronous) }
	
	// MARK: - NSViewController
	
	///
	/// The view with the provided orders that could be applied to the URLs.
	///
	@IBOutlet weak
	var orderTableView: OrderTableView!
	
	///
	/// Updates the order table view with the orders from the gare service.
	///
	override
	func viewDidLoad() {
		super.viewDidLoad()
		
		updateOrderTableView()
	}
	
	// MARK: -
	
	///
	/// Retrieves the orders from the service and adds them to the view.
	///
	public
	func updateOrderTableView() {
		gareService?.withOrders { orders in
			DispatchQueue.main.async { self.orderTableView.orders = orders.sorted { $0.title < $1.title } }
		}
	}
	
	// MARK: -
	
	///
	/// Closes the window.
	///
	@IBAction
	func handleOkAction(_ sender: NSButton) {
		view.window?.close()
	}
	
	///
	/// Opens the script folder with the Finder and close window/application.
	///
	@IBAction
	func handleOpenScriptFolder(_ sender: NSButton) {
		NSWorkspace.shared.open([Globals.scriptDirectory],
								withAppBundleIdentifier: nil,
								additionalEventParamDescriptor: nil,
								launchIdentifiers: nil)
		view.window?.close()
	}
	
}
