import Foundation
import AppKit

import GareShared

///
/// An action that can be executed by the Gare service.
///
public
protocol Action {
	
	///
	/// A unique identifier.
	///
	var id: ActionID { get }
	
	///
	/// The title derived from the file name.
	///
	var title: String { get }
	
	///
	/// The icon used by the finder for the script/file
	///
	var icon: NSImage { get }

	// MARK: -
	
	///
	/// Executes this action.
	///
	func execute(url anURL: URL)
	
}
