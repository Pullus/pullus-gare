import os.log
import Foundation

import PullusBase

import GareShared

///
/// Main implementation of the Gare extensions.
///
public
class GareService : GareServiceProtocol {
	
	///
	/// A catalog of all provided actions.
	///
	private
	var catalog: [ActionID:Action] = [:]
	
	///
	/// The orders derived from the actions.
	///
	private
	var orders: [Order] { return catalog.values.map(Order.init(action:)) }
	
	// MARK: -
	
	public
	init() {
		catalog = generateCatalog() ?? [:]
	}
	
	///
	/// Reads the script directory and generates a new catalog with new actions.
	///
	/// Actions that were included in the old catalog keep their id in the new
	/// catalog.
	///
	private
	func generateCatalog() -> [ActionID:Action]? {
		return try? FileManager.default
			// Fetch all visible files from the script directory
			.contentsOfDirectory(
				at: Globals.scriptDirectory,
				includingPropertiesForKeys: nil,
				options: [.skipsHiddenFiles])
			
			// Select only executable files
			.filter { FileManager.default.isExecutableFile(atPath: $0.path) }
			
			// Build actions
			.map { BashAction(scriptFileURL: $0) }
			
			// Sort out failed actions
			.compacted()
			
			// Create catalog
			.mapToDictionary { ($0.id, $0) }
	}
	
	// MARK: - GareServiceProtocol
	
	public
	func withOrders(reply aReply: ([Order]) -> Void) {
		aReply(orders)
	}
	
	public
	func execute(id anID: ActionID, url anURL: URL) {
		catalog[anID]?.execute(url: anURL)
	}
	
}
