import Foundation

import GareShared

///
/// Used to customize the `NSXPCListener` to bind incomming connections to new
/// service instances.
///
class ServiceListenerDelegate: NSObject, NSXPCListenerDelegate {
	
	///
	/// Establishes a connection to a new service instance.
	///
	/// This method is where the NSXPCListener configures, accepts, and resumes
	/// a new incoming NSXPCConnection.
	///
	func listener(_ aListener: NSXPCListener, shouldAcceptNewConnection aConnection: NSXPCConnection) -> Bool {
		// Configure the connection.
		// First, set the interface that the exported object implements.
		aConnection.exportedInterface = NSXPCInterface(with: GareServiceProtocol.self)
		
		// Add allowed/expected custom types.
		aConnection.exportedInterface?.configureGareServiceProtocol()
		
		// Next, set the object that the connection exports. All messages sent
		// on the connection to this service will be sent to the exported object
		// to handle. The connection retains the exported object.
		aConnection.exportedObject = GareService()
		
		// Resuming the connection allows the system to deliver more incoming
		// messages.
		aConnection.resume()
		
		// Returning YES from this method tells the system that you have
		// accepted this connection. If you want to reject the connection for
		// some reason, call -invalidate on the connection and return NO.
		return true
	}
	
}
