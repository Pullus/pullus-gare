import Foundation

extension Process {
	
	///
	/// See: [StackOverflow](https://stackoverflow.com/questions/32238366/running-shell-commands-in-swift#40102679)
	///
	public convenience
	init(bashCommand: String, arguments bashArguments: String...) {
		self.init()
		
		// TODO: Refactor handling of spaces in command path. E.g ".../Application Support/...". Avoid ..."'"... hack
		let bashCommandAndArguments = (["'" + bashCommand + "'" ] + bashArguments).joined(separator: " ")
		
		launchPath = "/usr/bin/env"
		arguments = ["bash", "-c", bashCommandAndArguments]
	}
	
}
