import Foundation

///
///
///
class Main {
	
	// MARK: - XPC Service
	
	// swiftlint:disable weak_delegate
	///
	/// The delegate for the service.
	///
	/// The delegate must be stored as a strong reference.
	///
	private
	let serviceDelegate: ServiceListenerDelegate
	// swiftlint:enable weak_delegate

	///
	/// The listener for this service. It will handle all incoming connections.
	///
	private
	let serviceListener = NSXPCListener.service()
	
	// MARK: -
	
	init() {
		serviceDelegate = ServiceListenerDelegate()
		serviceListener.delegate = serviceDelegate
	}
	
	// MARK: -
	
	///
	/// Starts the listener to span service instances.
	///
	/// This method does not return.
	///
	func run() {
		// Resuming the service listener starts this service. This method does
		// not return.
		serviceListener.resume()
	}
	
}

Main().run()
