import Foundation
import AppKit

import GareShared

///
/// Implementation of an action that uses `bash` to execute a script.
///
public
class BashAction : Action {
	
	private typealias SELF = BashAction
	
	// MARK: -
	
	public let id: ActionID
	public let title: String
	public var icon: NSImage
	
	private let scriptFileURL: URL
	
	// MARK: -
	
	public
	init?(scriptFileURL aScriptFileURL: URL) {
		guard let i = SELF.createID(aScriptFileURL) else { return nil }
		id = i
		
		title = SELF.createTitle(aScriptFileURL)
		scriptFileURL = aScriptFileURL
		icon = NSWorkspace.shared.icon(forFile: aScriptFileURL.path)
	}
	
	///
	/// Returns a unique identifier for an file URL.
	///
	/// Multiple calls with same URL return the same id (if no error occures).
	///
	/// If an error occurs, 0 is returned (e.g. file doesn't exists)
	///
	private
	static func createID(_ aScriptFileURL: URL) -> ActionID? {
		guard
			let fileAttributes = try? FileManager.default.attributesOfItem(atPath: aScriptFileURL.path),
			let id = fileAttributes[.systemFileNumber] as? NSNumber
			else { return nil }
		
		return id
	}
	
	///
	/// Creates a title from an URL
	///
	/// Returns the base name without the extension.
	///
	private
	static func createTitle(_ anURL: URL) -> String {
		return anURL.deletingPathExtension().lastPathComponent
	}
	
	// MARK: -
	
	public
	func execute(url anURL: URL) {
		let process = Process(
			bashCommand: scriptFileURL.path,
			arguments: anURL.absoluteString)
		
		process.launch()
		process.waitUntilExit()
	}
	
}
