import GareShared

///
///
///
extension Order {
	
	public convenience
	init(action anAction: Action) {
		self.init(id: anAction.id, title: anAction.title, icon: anAction.icon)
	}
	
}
