// swift-tools-version:4.0
import PackageDescription
import Foundation

// TODO: Implement the Swift Package Manager build of GARE

///
/// The Swift Package Manager build is not implemented.
///
/// This package description is only a skeleton to compile and check the
/// sources.
///
/// Build this package with minimum platform MacOS 10.12 by using the `target`
/// `x86_64-apple-macosx10.12`
///
/// Call
/// ```
///    swift build -Xswiftc "-target" -Xswiftc "x86_64-apple-macosx10.12"
/// ```
///
// swiftlint:disable trailing_comma
let package = Package(
	name: "Pullus-Gare",
	products: [
		.library(
			name: "Gare",
			targets: ["PullusBase"]),
		.library(
			name: "GareExtension",
			targets: ["GareExtension"]),
		.library(
			name: "GareService",
			targets: ["GareService"]),
		],
	
	dependencies: [
	],
	
	targets: [
		.target(
			name: "Gare",
			dependencies: [
				.target(name: "GareShared"),
				.target(name: "PullusBase"),
				],
			path: "Gare"
		),
		.target(
			name: "GareExtension",
			dependencies: [
				.target(name: "GareService"),
				.target(name: "GareShared"),
				.target(name: "PullusBase"),
				],
			path: "GareExtension"
		),
		.target(
			name: "GareService",
			dependencies: [
				.target(name: "GareShared"),
				.target(name: "PullusBase"),
				],
			path: "GareService"
		),
		.target(
			name: "GareShared",
			dependencies: [
				.target(name: "PullusBase"),
				],
			path: "GareShared"
		),
		.target(
			name: "PullusBase",
			dependencies: [
			],
			path: "Frameworks/Pullus-Base/PullusBase"),
		]
)
// swiftlint:enable weak_delegate
