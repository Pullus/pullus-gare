#!/bin/bash

function build_icon {
	basename=$1
	size=$2
	variant=$3
	rsvg-convert -w ${size} -h ${size} -f png -o "${basename}-${variant}.png" ${basename}.svg
}

build_icon Gare   16 16
build_icon Gare   32 16@2x
build_icon Gare   32 32
build_icon Gare   64 32@2x
build_icon Gare  128 128
build_icon Gare  256 128@2x
build_icon Gare  256 256
build_icon Gare  512 256@2x
build_icon Gare  512 512
build_icon Gare 1024 512@2x
